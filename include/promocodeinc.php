<?php
	define ("DB_PATH", "db/promocodes.db");

	class PromoCodeManager{
	
		// if this is the first run we'll need to set some things up
		public $firstRun = "false";
		public $claimedCode = "false";
		
		public function __construct()
		{
			$this->firstRun = $this->checkFirstRun();
			
			// see if we're posting something
			if (isset($_POST['promocodes'])){
	
				if ($this->checkPassword($_POST['pass'])){
					$this->insertPromoCodes($_POST['promocodes']);
				}
				else{
					print("<span>The password was incorrect.</span>");
				}
			}
			else if (isset($_POST['promocode'])){
				$this->claimPromoCode($_POST['promocode']);
			}
		}
		
		function checkFirstRun(){
			// triggers off of whether or not the database file exists
			if (!file_exists(DB_PATH)){
				echo "Looks like we need to set some things up here...</br>";
				
				if (!$this->checksqlite()){
					return "false";
				}
				
				$this->createDatabase();
				return "true";
			}
			else
				return "false";
		}
		
		function checkSqlite(){
			try{
				sqlite_libversion();
			}
			catch (Exception $e){
				echo "You may not have the sql lite PHP extension installed! <br/>";
				return false;
			}
			return true;
		}
		
		function createDatabase(){
			echo "Initializing Database... <br/>";
			
			$dbhandle = sqlite_open(DB_PATH, 0666, $error);
			if (!$dbhandle) die ("Database error: " .$error . "<br/>");

			// add the codes table
			$stm = "CREATE TABLE Codes(" . 
				   "code varchar(25) PRIMARY KEY, " . 
				   "used integer );";
				   
			$ok = sqlite_exec($dbhandle, $stm, $error);	

			if (!$ok)
			   die("Cannot execute query. $error");

			echo "Database created successfully";

			sqlite_close($dbhandle);
		}
		
		function insertPromoCodes($codeString){
			// split the codes into an array
			$codes = explode("\n", $codeString);
			
			// open the database
			$dbhandle = sqlite_open(DB_PATH, 0666, $error);
			if (!$dbhandle) die ("Database error: " .$error . "<br/>");
			
			// clear out the existing codes
			$stm = "delete from codes;";
			$ok = sqlite_exec($dbhandle, $stm, $error);
			
			
			for ($i=0; $i < count($codes); $i++){
				// strip whitespace
				$code = preg_replace('/\s+/', '', $codes[$i]);
				
				if ($code<>''){
					$stm = "insert into codes (code, used) values ('".sqlite_escape_string($code)."', 0);";
					$ok = sqlite_exec($dbhandle, $stm, $error);
				
					if (!$ok)
						die("Cannot execute query. $error");
				}
			}
		}
				
		function checkPassword($password){
			return ($password == ADMIN_PASSWORD);
		}
		
		function getRandomCode(){
			// open the database
			$dbhandle = sqlite_open(DB_PATH, 0666, $error);
			if (!$dbhandle) die ("Database error: " .$error . "<br/>");
			
			$query = "SELECT code FROM Codes WHERE used=0";
			$result = sqlite_query($dbhandle, $query);			
			if (!$result) die("Cannot execute query.");
			
			$rows = sqlite_num_rows($result);
			$randRow = rand(0, $rows-1);
			
			// not sure the best way to do this, I guess I'll cheat for now
			$row = sqlite_fetch_array($result); 
			for ($i=0;$i<$randRow;$i++){
				$row = sqlite_fetch_array($result); 
			}
			
			return $row['code'];			
		}
		
		function claimPromoCode($code){
			// open the database
			$dbhandle = sqlite_open(DB_PATH, 0666, $error);
			if (!$dbhandle) die ("Database error: " .$error . "<br/>");
			
			$stm = "update Codes set used=1 where code='".sqlite_escape_string($code)."';";
			$ok = sqlite_exec($dbhandle, $stm, $error);
			
			if (!$ok){
				die("Cannot execute query. $error");
			}
			
			$this->claimedCode = "true";
		}
	}
?>
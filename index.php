<?php
	// configuration 
	define ("APP_NAME", "mediaPOST");
	define ("APP_PAGE", "http://mediapost.coffeebeanstudios.com");
	define ("CONTACT_EMAIL", "claude@coffeebeanstudios.com");
	define ("ADMIN_PASSWORD", "promocodeadmin");
	
	
	include("include/promocodeinc.php");
	$mgr = new PromoCodeManager;
?>

<html>
<head>
	<title><?php echo APP_NAME; ?> Promo Codes</title>
	<link type='text/css' rel="stylesheet" href='css/codes.css'>
	<script src="js/jquery-1.8.2.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			
			$("#setup").hide();
			$("#thanksDiv").hide();			
  
			$("#saveCodesBtn").click(function(){
				$("#setupForm").submit();
			});
			
			$("#claimBtn").click(function(){
				$("#claimForm").submit();
			});
			
			$("#adminDiv").click(function(){
				$("#getcode").hide();
				$("#setup").show();
			});
			
			// grab the first run variable from the php
			var firstRun = <?php echo $mgr->firstRun; ?>;
			
			
			if (firstRun){
				$("#getcode").hide();
				$("#setup").show();			
			}	
						
			var randomCode = '<?php echo $mgr->getRandomCode(); ?>';
			if (randomCode==''){
				$("#promocode").val("Sorry, out of codes!");
			}
			else{
				$("#promocode").val(randomCode);
			}
			
			var claimedCode = <?php echo $mgr->claimedCode; ?>;
			
			if (claimedCode){
				$("#getcode").hide();
				$("#setup").hide();	
				$("#thanksDiv").show();
			}
		});
	
	</script>		
</head>
<body>
	<div id="getcode" class="content">
		<span class="appname"><?php print(APP_NAME . " Promo Codes"); ?></span><br/>
		<a href="<? echo APP_PAGE; ?>"><img class="appicon" src="img/icon.png"/></a><br/>
		<form id="claimForm" name="claimForm" method="post">
			<input id="promocode" name="promocode" class="promocode" type="text" value=""></input><br/>
			<span id="claimBtn" class="claimbutton">Claim Promo Code</span><br/>
		</form>
	</div>
	
	<div id="setup" class="content" style="visibilty:hidden">
		<span class="appname">Paste your promo codes below</span><br/>		
		<form id="setupForm" method="post">
			<textarea id="promocodes" name="promocodes" class="promocode" type="text" cols="40" rows="3"></textarea><br/>
			<label id="passwordLbl" for="pass" class="appname">Password</label><input type="password" name="pass" class="promocode"></input><br/>
			<span id="saveCodesBtn" class="claimbutton">Save</span><br/>
		</form>
	</div>
	
	<div id="thanksDiv" class="content" style="visibilty:hidden">
		<span class="appname">Thanks for trying <?php echo APP_NAME; ?>!</span><br/><br/>	
		<a href="<? echo APP_PAGE; ?>"><img class="appicon" src="img/icon.png"/></a><br/><br/>
		<span class="notes">If you have any questions or comments please contact me at <a href="mailto:<?php echo CONTACT_EMAIL; ?>"><?php echo CONTACT_EMAIL; ?>.</a></span>
	</div>
	
	<div id="adminDiv">
		<img class="appicon" src="img/gear.png"/>
	</div>
	
</body>
</html>
Promo Code Manager
=========

Use this promo code manager to divy out your promotional codes. Save people time from trying every code in a huge list.

  - PHP + Javascript
  - SQLite backend (means no installation)
  - It's pretty (at least I think so)
  
Example
-
[Here is a good example](http://mediapost.coffeebeanstudios.com/codes/)

Installation
-
Download the code, drop it in a folder on your webserver and navigate to the URL in a browser. It will prompt you for the promo codes and password (defaults to 'promocodeadmin'), then you're ready to go. 

Configuration
-
You can configure this to perfectly match your application. App name, website and a couple other things can be configured in the constants on index.php.
A custom icon can be used by replacing img/icon.png with the icon for your app.

Requirements
-
	- Webserver with PHP installed
	- SQLite PHP module (Should be enabled by default on shared hosts)
	- http://php.net/manual/en/sqlite.installation.php

License
-
MIT